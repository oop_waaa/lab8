package com.patcharin.week8;

public class Rectangle {
    public int width;
    public int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    
    }

    public int areas() {
        int areas = width*height;
        return areas;
    }

    public int perimeter() {
        int perimeter = 2*(width+height);
        return perimeter;
    }
}
