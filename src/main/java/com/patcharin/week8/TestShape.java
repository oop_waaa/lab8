package com.patcharin.week8;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10,5);
        Rectangle rect2 = new Rectangle(5,3);
        Circle circle1 = new Circle(1);
        Circle circle2 = new Circle(2);
        Triangle triangle = new Triangle(5,5,6);

        System.out.println("Rectangle area1 = "+rect1.areas());
        System.out.println("Rectangle area2 = "+rect2.areas());
        System.out.println("Circle area1 = "+circle1.areas());
        System.out.println("Circle area2 = "+circle2.areas());
        System.out.println("Triangle area = "+triangle.areas());
        
        System.out.println("Rectangle perimeter1 = "+rect1.perimeter());
        System.out.println("Rectangle perimeter2 = "+rect2.perimeter());
        System.out.println("Circle perimeter1 = "+circle1.perimeter());
        System.out.println("Circle perimeter2 = "+circle2.perimeter());
        System.out.println("Triangle perimeter = "+triangle.perimeter());

    }
}
