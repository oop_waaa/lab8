package com.patcharin.week8;

public class Circle {
    public  double radius;

    public Circle(double radius) {
        this.radius = radius;

    }

    public  double areas() {
        double areas = Math.PI*(radius*radius);
        return areas;
    }

    public double perimeter() {
        double perimeter = 2*Math.PI*radius;
        return perimeter;
    }
}
