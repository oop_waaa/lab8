package com.patcharin.week8;

public class Map2 {
    private int width;
    private int height;

    public Map2() {
        this.width = 10;
        this.height = 10;
    }
    public Map2(int x,int y) {
        this.width = x;
        this.height = y;
    
    }
    public int getwidth() {
        return width;
    }
    public int getheight() {
        return height;
    }
    public void print() {
        for(int i=0;i<height; i++) {
            for(int j=0; j< width ; j++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }

}
