package com.patcharin.week8;

public class Map {
    private int width;
    private int height;

    public Map() {
        this.width = 5;
        this.height = 5;
    }
    public Map(int x,int y) {
        this.width = x;
        this.height = y;
    
    }
    public int getwidth() {
        return width;
    }
    public int getheight() {
        return height;
    }
    public void print() {
        for(int i=0;i<height; i++) {
            for(int j=0; j< width ; j++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }
}
